FROM python:3.8
ENV PYTHONUNBUFFERED 1
ENV C_FORCE_ROOT true
RUN mkdir /nginx
RUN mkdir /site
RUN mkdir -p /site/static
RUN mkdir -p /site/src
WORKDIR /site
RUN rm -f /src/celerybeat.pid
COPY ./src/requirements.txt .
RUN pip install -r requirements.txt

WORKDIR /site/src