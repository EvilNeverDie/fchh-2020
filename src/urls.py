"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.static import serve

from apps.core.views import *
from settings import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/users/', UserView.as_view()),
    path('api/v1/availability/', calculate_availability),
    path('api/v1/devices/list/', DevicesApiView.as_view()),
    path('api/v1/device_types/list/', DeviceTypesApiView.as_view()),
    path('api/v1/user_templates/', UserTemplatesAPIView.as_view()),
    path('api/v1/feedback/', give_feedback),
    path('api/v1/add_device/', upload_device),
    path('api/v1/get_impulse_character/<int:pk>/', get_impuls_list)
]
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
else:
    urlpatterns += [
        path('media/<path>', serve, {'document_root': settings.MEDIA_ROOT, }),
        path('static/<path>', serve, {'document_root': settings.STATIC_ROOT})
    ]
