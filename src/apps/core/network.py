from __future__ import division

from typing import List

import numpy as np
from statsmodels.tsa.stattools import ccf
import re



def get_ccf_from_dumps(dump_in: List[str], dump_out: List[str]):
    enter = []
    exit = []
    for line_in, line_out in zip(dump_in[:-2], dump_out[:-2]):
        line_in, line_out = str(line_in), str(line_out)
        try:
            enter.append(float(line_in[8:16]))
            exit.append(float(line_in[8:16]))
        except:
            continue
    ew1 = []
    eww1 = []
    for i in range(0, len(enter) - 1):
        delta = enter[i + 1] - enter[i]
        delta1 = exit[i + 1] - exit[i]
        if delta > 0:
            ew1.append(delta)
        if delta1 > 0:
            eww1.append(delta1)
    dd1 = ccf(ew1, eww1, adjusted=True)
    return dd1
