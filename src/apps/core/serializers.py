from django.contrib.auth.hashers import make_password
from rest_framework import serializers

from apps.core.models import CustomUser, Device, UserTemplate, DeviceType


class UserCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['email', 'password']

    def create(self, validated_data):
        user = self.Meta.model(**validated_data)
        user.password = make_password(validated_data['password'])
        user.save()
        return user


class DeviceTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeviceType
        exclude = ['image']


class DeviceImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeviceType
        fields = ['image']


class DeviceListSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    device_type = DeviceTypeSerializer()

    def get_image(self, obj):
        return DeviceImageSerializer(obj.device_type, context=self.context).data['image']

    class Meta:
        model = Device
        fields = ['id', 'device_type', 'name', 'image']


class UserGetSerializer(serializers.ModelSerializer):
    chat_token = serializers.SerializerMethodField()

    def get_chat_token(self, obj):
        return obj.chat_token

    class Meta:
        model = CustomUser
        fields = ['id', 'email', 'chat_token']


class UserTemplateGETSerializer(serializers.ModelSerializer):
    template = serializers.SerializerMethodField()

    def get_template(self, obj):
        res = {}
        template = obj.template
        res['id'] = obj.id
        request = self.context.get('request')
        res['image'] = f'http://{request.get_host()}/media/{obj.image}/'
        for i in template['nodes'].items():
            device_id = i[1]["device_id"]
            device_data = DeviceListSerializer(instance=Device.objects.get(id=device_id), context=self.context).data
            template['nodes'][i[0]]['device'] = device_data
        res['nodes'] = template['nodes']
        res['edges'] = template['edges']
        return res

    class Meta:
        model = UserTemplate
        fields = ['template']
