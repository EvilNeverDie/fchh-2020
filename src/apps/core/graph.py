import math
from datetime import datetime, timedelta
from typing import Dict, List, Optional

import sympy as sym
from rest_framework.exceptions import APIException
from scipy.special import kolmogorov
import networkx as nx
import random
import numpy as np
from scipy.stats import kstest
from sympy import sqrt

from apps.core.models import Device
from apps.core.serializers import DeviceListSerializer


def time_generator():
    res = []
    for i in range(5000):
        ms = random.randint(1, 100)
        res.append(ms)
    return res


class CustomGraph:
    def __init__(self, raw_graph: Dict):
        self.raw_graph = raw_graph
        self.direct_time = raw_graph['directive_time']
        self.matrix = None
        self.graph = None
        self.n = None
        self._build_networkx_graph()

    def _calculate_percent_arrived_for_direct_time(self,
                                                   traffic_intervals_list: List,
                                                   start_device_id,
                                                   finish_device_id):
        try:
            start_device = Device.objects.get(id=start_device_id)
            finish_device = Device.objects.get(id=finish_device_id)
        except:
            raise APIException(f"Can't get device")
        diff_intervals = np.diff(np.array(traffic_intervals_list), n=1, axis=-1)
        conv = np.convolve(diff_intervals, start_device.ccf, 'full')
        cond = [i for i in conv if i <= self.direct_time]
        availability_percent_start = round((len(cond) * 100 / len(conv)) * 0.01, 2)
        conv = np.convolve(diff_intervals, finish_device.ccf, 'full')
        cond = [i for i in conv if i <= self.direct_time]
        availability_percent_finish = round((len(cond) * 100 / len(conv)) * 0.01, 2)
        return availability_percent_start, availability_percent_finish

    def _build_networkx_graph(self):
        g = nx.MultiDiGraph()
        traffic_interval_list = time_generator()
        # add nodes
        for i in self.raw_graph['nodes'].keys():
            g.add_node(i)
        self.n = len(self.raw_graph['nodes'].keys())
        # add edges
        for i in self.raw_graph["edges"]:
            start = i['start']
            end = i['end']
            percent_in, percent_out = self._calculate_percent_arrived_for_direct_time(traffic_interval_list,
                                                                                      self.raw_graph['nodes'][start][
                                                                                          'device_id'],
                                                                                      self.raw_graph['nodes'][end][
                                                                                          'device_id'])
            g.add_edge(start, end, weight=percent_in)
            g.add_edge(end, start, weight=percent_out)
            i['to_end'] = percent_in
            i['to_start'] = percent_out
        self.graph = g

    def create_matrix(self):
        if not self.graph:
            raise ValueError("There is no graph")
        self.matrix = nx.to_numpy_matrix(self.graph)


    def create_matrix_h(self, matrix_t, min: Optional[bool] = False):
        if min:
            cond_of_change = (math.inf, 0)
        else:
            cond_of_change = (1, 0)
        matrix_h = np.squeeze(np.asarray(matrix_t.copy()))
        matrix_h.fill(0)
        for i in range(self.n):
            for j in range(self.n):
                if matrix_t[i][j] not in cond_of_change:
                    matrix_h[i][j] = j + 1
        return matrix_h

    def create_matrix_g(self, matrix_t):
        matrix_g = np.squeeze(np.asarray(matrix_t.copy()))
        for i in range(self.n):
            for j in range(self.n):
                if matrix_t[i][j] == 0:
                    matrix_g[i][j] = 0
                elif matrix_t[i][j] != math.inf:
                    matrix_g[i][j] = 1
                else:
                    matrix_g[i][j] = math.inf
        return matrix_g

    def channels_wit_max_accessibility(self):
        """
        Матрица смежности T, которая заполнена величинами доступности между соседними узлами,
        т.е. элемент матрицы T[i][j] содержит величину доступности исходящего из узла i ребра и
        входящего в узел j. Если два узла не связаны друг с другом на прямую, то доступность ставим
        равную нулю, а не бесконечности, т.к. необходимо найти путь с наибольшими весами,
        а не с наименьшими, как в исходном алгоритме Флойда-Уоршелла. Главную диагональ, соответственно,
        заполняем единицами, т.к. доступность узлов учитывать не нужно. Это связано с тем, что доступность
        узлов уже учтена в доступности каждого исходящего из них ребра. Таким образом, изначально матрица
        T содержит в себе только значения доступностей всех ребер сети.
        """
        matrix_t = self.matrix.copy()
        np.fill_diagonal(matrix_t, 1)
        matrix_t = np.squeeze(np.asarray(matrix_t))
        matrix_h = self.create_matrix_h(matrix_t)
        # а теперь по флойд уоршелу
        for k in range(self.n):
            for i in range(self.n):
                for j in range(self.n):
                    if (i != k) and (j != k) and (matrix_t[i][k] != 0) and \
                            (matrix_t[k][j] != 0) and \
                            (matrix_t[i][j] < (matrix_t[k][j] * matrix_t[i][k])):
                        matrix_t[i][j] = matrix_t[k][j] * matrix_t[i][k]
                        matrix_h[i][j] = k
        return matrix_t

    def channels_wit_min_accessibility(self):
        """
        Матрица смежности T, которая заполнена величинами доступности между соседними узлами,
        т.е. элемент матрицы T[i][j] содержит величину доступности исходящего из узла i ребра
        и входящего в узел j. Если два узла не связаны друг с другом на прямую, то доступность
        ставим равную бесконечности, а главную диагональ соответственно заполняем нулями.
        """
        matrix_t = self.matrix.copy()
        matrix_t = np.squeeze(np.asarray(matrix_t))
        matrix_t[matrix_t == 0] = math.inf
        np.fill_diagonal(matrix_t, 0)
        """
        Шаг №1. Получаем из матрицы смежности T матрицу H, по которой можно будет восстановить 
        путь от одной вершины до другой. Этот шаг алгоритма аналогичен шагу 2 из вышеописанного 
        алгоритма для поиска каналов сети с максимально возможной величиной доступности.
        """
        matrix_h = self.create_matrix_h(matrix_t, min=True)
        """
        Шаг №2. Получаем из матрицы смежности T матрицу смежности стандартного вида G. 
        """
        matrix_g = self.create_matrix_g(matrix_t)
        for k in range(self.n):
            for i in range(self.n):
                for j in range(self.n):
                    if (i != k) and (j != k) and (matrix_g[i][k] != math.inf) and \
                            (matrix_g[k][j] != math.inf) and \
                            (matrix_g[i][j] > (matrix_g[k][j] * matrix_g[i][k])):
                        matrix_g[i][j] = matrix_g[k][j] * matrix_g[i][k]
                        matrix_h[i][j] = matrix_h[i][k]
                        matrix_t[i][j] = matrix_t[k][j] * matrix_t[i][k]
        return matrix_t

    def get_availability_of_result_matrix(self, a):
        fin_sum = 0
        for j in range(self.n):
            sum_ = 0
            for i in range(self.n):
                res = 1 - a[i][j]
                ti = sym.Symbol(f't{i}')
                res_ = res * ti
                sum_ += res_
            sum_sqr = sum_ ** 2
            fin_sum += sum_sqr

        for i in [i for i in range(self.n)]:
            fin_sum = sym.integrate(fin_sum, (f"t{i}", 0, 1))
        return sqrt(fin_sum)

    def get_d_max_value(self):
        if self.n <= 50:
            d_max = (-0.000305962 * (self.n ** 3)) + (0.0628791 * (8 ** 2)) + (1.20093 * 8) - 1.8333
        else:
            d_max = (-3.38745 * 0.000001 * (self.n ** 3)) + (0.0140894 * (self.n ** 2)) + (5.2436 * self.n) - 146.096
        return d_max

    def get_availability(self) -> Dict:
        self.create_matrix()
        print(self.matrix)
        matrix_min = self.channels_wit_min_accessibility()
        matrix_max = self.channels_wit_max_accessibility()
        print(matrix_max)
        max_availability = self.get_availability_of_result_matrix(matrix_max)
        min_availability = self.get_availability_of_result_matrix(matrix_min)
        d_max_value = self.get_d_max_value()
        max_availability = math.exp(-1 * (max_availability / d_max_value))
        min_availability = math.exp(-1 * (min_availability / d_max_value))
        print(max_availability, min_availability)
        return {"max_availability": max_availability, "min_availability": min_availability}

    def serialize_devices(self, request):
        for i in self.raw_graph['nodes']:
            device_id = self.raw_graph['nodes'][i]['device_id']
            device = Device.objects.get(id=device_id)
            self.raw_graph['nodes'][i]['device'] = DeviceListSerializer(device, context={'request': request}).data
