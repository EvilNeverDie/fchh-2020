from datetime import datetime

import jwt
from django.contrib.auth.models import AbstractUser

# Create your models here.
from django.contrib.postgres.fields import JSONField
from django.core.validators import validate_email
from django.db import models
from django.contrib.postgres.fields import ArrayField


class CustomUser(AbstractUser):
    @property
    def chat_token(self) -> str:
        data = {"sub": f"{self.id}", "info": None}

        data.update({"exp": None})

        token = jwt.encode(
            data,
            "46b38493-147e-4e3f-86e0-dc5ec54f5133",
            algorithm="HS256",
        )
        return token.decode(encoding='utf-8')

    def save(self, *args, **kwargs):
        validate_email(self.email)
        self.username = self.email
        super(CustomUser, self).save(*args, **kwargs)


class DeviceType(models.Model):
    name = models.CharField(max_length=300, blank=False, null=False)
    is_final_device = models.BooleanField(default=False)
    image = models.ImageField(upload_to='device_images', blank=True, null=True)

    class Meta:
        verbose_name = 'Тип сетевого устройства'
        verbose_name_plural = 'Типы сетевых устройств'


class Device(models.Model):
    name = models.CharField(max_length=300, blank=False, null=False)
    device_type = models.ForeignKey(DeviceType, on_delete=models.SET_NULL, null=True)
    delay = models.FloatField(default=0.001)
    ccf = ArrayField(models.FloatField(blank=False, null=False), default=list)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        verbose_name = 'Сетевое устройство'
        verbose_name_plural = 'Сетевые устройства'


class UserTemplate(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, blank=True, null=True)
    template = JSONField(default=dict)
    result = JSONField(default=dict)
    date_time = models.DateTimeField(default=datetime.now)
    image = models.ImageField(upload_to='images', blank=True, null=True)

    class Meta:
        verbose_name = 'Шаблон'
        verbose_name_plural = 'Шаблоны'


class UserFeedback(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, blank=True, null=True)
    title = models.CharField(max_length=300, blank=False, null=False)
    body = models.TextField(blank=False, null=False)

    class Meta:
        verbose_name = 'Обратная связь'
        verbose_name_plural = 'Обратная связь'
