import random
from datetime import datetime, timedelta

import networkx as nx
import numpy as np
from cent import Client

from apps.core.models import Device
from settings.celery import app


@app.task()
def echo_task(channel):
    client = Client("http://centrifugo-sapr:8000", api_key="aaaf202f-b5f8-4b34-bf88-f6c03a1ecda6", timeout=1)
    client.publish(channel, {"message": "Hello, your task was complete successfully!"})



def get_networkx_graph(data):
    g = nx.Graph()
    for i in data['edges']:
        g.add_edge(i["start"], i["end"])
    nodes = {int(i[0]): i[1] for i in data['nodes'].items()}
    nx.set_node_attributes(g, nodes)
    return g


def get_network_delay(data):
    graph = get_networkx_graph(data)
    device_id_list = [i['device_id'] for i in data['nodes'].values()]
    device_delay_dict = { i[0]: i[1] for i in Device.objects.filter(id__in=device_id_list).values_list("id", "delay")}
    edges_ = []
    graph_device_id = nx.get_node_attributes(graph, 'device_id')
    for i in graph.edges():
        edge = {"start": i[0], "end": i[1]}
        delay = device_delay_dict[graph_device_id[i[0]]] + device_delay_dict[graph_device_id[i[1]]]
        edge['value'] = delay
        edges_.append(edge)
    return edges_


def time_generator():
    res = []
    current = datetime.now()
    res.append(str(current.time()))
    for i in range(2000):
        ms = random.randint(80000,130000)
        current += timedelta(microseconds=ms)
        res.append(str(current.time()))
    return res

def process_dump(dump):
    result = []
    for line in dump:
        result.append(float(line[6:-1]))
    ew1 = []
    for i in range(0, len(result) - 1):
        delta = result[i + 1] - result[i]
        if delta > 0:
            ew1.append(delta)
    return ew1

def calculate_signal_first(dd1, ew1):
    diff_ew1 = np.diff(np.array(ew1), n=1, axis=-1)
    diff_ix1 = np.diff(np.array(dd1), n=1, axis=-1)
    conv = np.convolve(diff_ix1[:len(diff_ew1)], diff_ew1, 'full')
    return conv

def get_new_network_delay(data):
    time_in = time_generator()
    processed_dump = process_dump(time_in)
    # строим граф
    graph = get_networkx_graph(data)
    # получаем общий список устройств
    device_id_list = [i['device_id'] for i in data['nodes'].values()]
    device_list = Device.objects.filter(id__in=device_id_list)
    # получаем список конечных устроиств
    final_device_list = device_list.filter(device_type__is_final_device=True).values_list("id", flat=True)
    # получаем список маршрутизаторов
    router_device_list = {i['id']: i['ccf'] for i in device_list.filter(device_type__is_final_device=False).values("id", "ccf")}
    delays = []
    for node in nx.all_pairs_shortest_path(graph):
        print(node[0], data['nodes'])
        if data['nodes'][str(node[0])]['device_id'] in final_device_list:
            for finish in node[1].items():
                if finish[0] == node[0]:
                    continue
                if data['nodes'][str(finish[0])]['device_id'] not in final_device_list:
                    continue
                conv = []
                for index, i in enumerate(finish[1][1:-1]):
                    if index == 0:
                        print(router_device_list, len(list(conv)))
                        conv = calculate_signal_first(router_device_list[data['nodes'][str(i)]['device_id']], processed_dump)
                        continue
                    print(router_device_list, len(list(conv)))
                    conv = calculate_signal_first(router_device_list[data['nodes'][str(i)]['device_id']], conv)
                delays.append(abs(sum(j-i for i,j in zip(processed_dump, conv))/
                                  len(list(zip(processed_dump, conv)))) - 0.1)
    return sum(delays)/len(delays) + 0.01 * len(list(router_device_list.values()))

