import io
from datetime import datetime

import networkx as nx
from django.contrib.auth.models import AnonymousUser
from django.core.files.images import ImageFile
from django.db.models import Q
from matplotlib import pyplot as plt

from rest_framework.decorators import action, api_view, permission_classes
from rest_framework.generics import ListAPIView, ListCreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.core.graph import CustomGraph
from apps.core.models import Device, UserTemplate, UserFeedback, DeviceType
from apps.core.network import get_ccf_from_dumps
from apps.core.serializers import UserCreateSerializer, DeviceListSerializer, UserGetSerializer, \
    UserTemplateGETSerializer, DeviceTypeSerializer
from apps.core.tasks import echo_task, get_networkx_graph, get_network_delay, get_new_network_delay


class UserView(APIView):

    def post(self, request, *args, **kwargs):
        serializer = UserCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response(UserGetSerializer(instance=user).data, status=201)

    def get(self, request, *args, **kwargs):
        if isinstance(request.user, AnonymousUser):
            return Response(status=404)
        return Response(UserGetSerializer(instance=request.user).data, status=200)


class DevicesApiView(ListAPIView):
    queryset = Device.objects.all()
    serializer_class = DeviceListSerializer

    def get_queryset(self):
        self.queryset = Device.objects.filter(Q(user__isnull=True) | Q(user=self.request.user))
        return self.queryset


class DeviceTypesApiView(ListAPIView):
    queryset = DeviceType.objects.all()
    serializer_class = DeviceTypeSerializer
    pagination_class = None

class UserTemplatesAPIView(ListCreateAPIView):
    queryset = UserTemplate.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = UserTemplateGETSerializer

    def create(self, request, *args, **kwargs):
        template = UserTemplate.objects.create(template=request.data, user=request.user)
        g = get_networkx_graph(request.data)
        plt.clf()  # which clears data and axes
        plt.subplot()
        nx.draw(g, with_labels=True, font_weight='bold', font_size=8, node_size=500)
        a = io.BytesIO()
        plt.savefig(a, format="png")
        content_file = ImageFile(a)
        template.image.save('image.png', content_file)
        queryset = self.queryset.filter(user=request.user).order_by("-date_time")
        data = self.serializer_class(queryset, many=True, context={"request": request}).data
        response_data = [i['template'] for i in data]
        return Response(response_data)

    def list(self, request, *args, **kwargs):
        queryset = self.queryset.filter(user=request.user).order_by("-date_time")
        data = self.serializer_class(queryset, many=True, context={"request": request}).data
        response_data = [i['template'] for i in data]
        return Response(response_data)


@api_view(http_method_names=['POST'])
@permission_classes([IsAuthenticated])
def give_feedback(request):
    UserFeedback.objects.create(user=request.user, title=request.data['title'], body=request.data['body'])
    return Response(status=201)


@api_view(http_method_names=['POST'])
def calculate_availability(request):
    custom_graph = CustomGraph(request.data)
    res = custom_graph.get_availability()
    custom_graph.serialize_devices(request)
    graph = custom_graph.raw_graph
    graph['availability'] = res
    return Response(graph)


@api_view(http_method_names=['POST'])
def upload_device(request):
    name = request.data.get('name')
    device_type_id = request.data.get('device_type_id')
    print(request.data.keys())
    print(request.FILES)
    shortest_range = 99999999999999999
    ccfs = []
    for i in range(int(len(request.FILES) / 2)):
        in_ = request.FILES[f'in_{i}']
        out_ = request.FILES[f'out_{i}']
        ccf = get_ccf_from_dumps(in_.readlines(), out_.readlines())
        if len(ccf) < shortest_range:
            shortest_range = len(ccf)
        ccfs.append(ccf)
    ccfs = [i[:shortest_range] for i in ccfs]
    ccf_final = []
    for i in range(shortest_range):
        sum_ = 0
        for j in ccfs:
            sum_ += j[i]
        ccf_final.append(sum_ / len(ccfs))
    device = Device.objects.create(name=name, device_type_id=device_type_id,
                                   user=request.user, ccf=ccf_final)
    return Response(DeviceListSerializer(instance=device,
                                         context={"request": request}).data, status=201)


@api_view(http_method_names=['GET'])
def get_impuls_list(request, pk):
    device = Device.objects.get(id=pk)
    return Response({"IH": device.ccf})
