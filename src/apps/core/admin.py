from django import forms
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.forms import Form, ModelForm

from apps.core.models import Device, DeviceType, CustomUser, UserTemplate, UserFeedback
from apps.core.network import get_ccf_from_dumps


admin.site.register(CustomUser, ModelAdmin)
admin.site.register(UserTemplate, ModelAdmin)


class DeviceUserCreationForm(ModelForm):
    dump_in = forms.FileField(required=True)
    dump_out = forms.FileField(required=True)

    class Meta:
        model = Device
        fields = ('name', 'device_type')

class DeviceTypeAdmin(ModelAdmin):
    list_display = ['name']

class FeedbackAdmin(ModelAdmin):
    list_display = ['title']

class DeviceCustomAdmin(ModelAdmin):
    add_form = DeviceUserCreationForm
    model = Device
    list_display = ['name']

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('name', 'device_type')}
         ),
    )

    def get_form(self, request, obj=None, **kwargs):
        defaults = {}
        if obj is None:
            defaults['form'] = self.add_form
        defaults.update(kwargs)
        return super().get_form(request, obj, **defaults)

    def save_model(self, request, obj, form, change):
        if obj.id is None:
            raw = form.cleaned_data
            file_in = list(raw['dump_in'].readlines())
            file_out = list(raw['dump_out'].readlines())
            ccf = get_ccf_from_dumps(file_in, file_out)
            obj.ccf = list(ccf)
        super(DeviceCustomAdmin, self).save_model(request, obj, form, change)


admin.site.register(Device, DeviceCustomAdmin)
admin.site.register(DeviceType, DeviceTypeAdmin)
admin.site.register(UserFeedback, FeedbackAdmin)