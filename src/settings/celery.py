import os

from celery.schedules import crontab
from django.conf import settings
from celery import Celery



class CeleryConfig(object):
    broker_url = 'redis://redis-sapr:6379/1'
    result_backend = 'redis://redis-sapr:6379/1'
    redis_host = "redis-web"
    worker_send_task_events = True
    timezone = 'Europe/Moscow'
    worker_disable_rate_limits = True


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings.settings')

app = Celery('apps')
app.config_from_object(CeleryConfig)
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)