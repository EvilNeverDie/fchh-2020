build:
	docker-compose build

up:
	docker-compose up -d

up-non-daemon:
	docker-compose up

start:
	docker-compose start

stop:
	docker-compose stop

restart:
	docker-compose stop && docker-compose start

shell-web:
	docker exec -ti web_kids bash

shell-db:
	docker exec -ti postgresql_kids bash

log-web:
	docker-compose logs web

log-db:
	docker-compose logs postgresql

makemigrations_merge:
	docker exec web_kids /bin/sh -c "python manage.py makemigrations --merge"

makemigrations:
	docker exec web_kids /bin/sh -c "python manage.py makemigrations"

migrate:
	docker exec web_kids /bin/sh -c "python manage.py migrate"
